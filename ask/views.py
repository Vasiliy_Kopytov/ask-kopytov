from django.shortcuts import render

def index(req):
	return render(req, 'index.html')

def signup(req):
	return render(req, 'signup.html')

def login(req):
	return render(req, 'login.html')


# Create your views here.
