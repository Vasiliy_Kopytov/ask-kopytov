from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:

    #url(r'^$', 'ask_kopytov.views.index', name='index'),

    # url(r'^blog/', include('blog.urls')),
    #url(r'^$', ask.views.index),
    url(r'^login/$', 'ask.views.login'),
    url(r'^signup/$', 'ask.views.signup'),
    url(r'^$', 'ask.views.index'),
    
    
    

    url(r'^admin/', include(admin.site.urls)),
)
