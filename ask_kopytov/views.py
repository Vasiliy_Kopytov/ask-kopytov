from django.shortcuts import render
from django.http import HttpResponse
from cgi import parse_qs


def index(request):
	status = '200 OK'

	output = '<html><body>Hello World 5! <br>'

	GET = request.GET

	output += "GET: <br> "
	for key in GET:
		output += " " + key + " = " + GET[key]+ " <br>"

	POST = request.POST
	output += "POST: <br>"
	for key in POST:
		output += " " + key + " = " + POST[key]+ " <br>"
	output += "</body></html>"

	return HttpResponse([output])


#def index(req):
#	return render(req, 'index.html')